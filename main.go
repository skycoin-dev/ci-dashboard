package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	yaml "gopkg.in/yaml.v2"
)

var (
	htmlTemplate = flag.String("html", "", "present report in html format using provided template")
	resultFile   = flag.String("out", "", "location of a report file. by default will write to stdout")
)

func main() {
	flag.Usage = func() {
		fmt.Fprintln(flag.CommandLine.Output(), "Usage ci-dashboard [flags] [config.yml]:")
		flag.PrintDefaults()
	}
	flag.Parse()

	configPath := "config.yml"
	if args := flag.Args(); len(args) != 0 {
		configPath = args[0]
	}

	config, err := parseConfig(configPath)
	if err != nil {
		log.Fatal("Failed to parse yaml config: ", err)
	}

	checker := NewChecker()
	report := checker.Check(config)

	if *htmlTemplate != "" {
		err = report.WriteHTML(*htmlTemplate, *resultFile)
	} else {
		err = report.Write(*resultFile)
	}

	if err != nil {
		log.Fatal("Failed to write report file: ", err)
	}
}

func parseConfig(filepath string) (*Config, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, fmt.Errorf("failed to open file: %s", err)
	}
	defer file.Close()

	config := &Config{}
	if err := yaml.NewDecoder(file).Decode(&config); err != nil {
		return nil, fmt.Errorf("failed to decode yml: %s", err)
	}

	return config, nil
}
