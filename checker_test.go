package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCheckScript(t *testing.T) {
	step := &CheckStep{"foo", false, []string{"go test", "go build"}}
	script := `set -e
go get -d -u -v foo
go test foo
go build foo`
	assert.Equal(t, script, step.Script())
}

func TestCheckScriptWithGlob(t *testing.T) {
	step := &CheckStep{"foo", true, []string{"go test", "go build"}}
	script := `set -e
go get -d -u -v foo/...
go test foo/...
go build foo/...`
	assert.Equal(t, script, step.Script())
}

func TestChecker(t *testing.T) {
	c := &Checker{&MockScriptRunner{}}
	steps := []*CheckStep{
		&CheckStep{"gitlab.com/skycoin-dev/skycoin-node-sync-test", false, []string{"go test"}},
	}
	report := c.Check(&Config{steps})
	require.Len(t, report.PackageResults, 1)

	result := report.PackageResults[0]
	assert.True(t, result.Success)
	assert.Equal(t, "gitlab.com/skycoin-dev/skycoin-node-sync-test", result.Package)
	assert.Empty(t, result.Error)
	assert.Equal(t, "foo", result.StdOut)
	assert.Equal(t, "bar", result.StdErr)
}

type MockScriptRunner struct {
	error string
}

func (sr *MockScriptRunner) Run(script string) (string, string, error) {
	if sr.error != "" {
		return "foo", "bar", errors.New(sr.error)
	}

	return "foo", "bar", nil
}
